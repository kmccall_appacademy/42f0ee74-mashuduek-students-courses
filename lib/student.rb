require 'byebug'
require "course.rb"
class Student

  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    @courses.each do |old_course|
      raise 'CANNOT DOUBLE-ENROLL' if old_course.conflicts_with?(new_course)
    end

    self.courses << new_course unless self.courses.include?(new_course)
    new_course.students << self

  end

  def course_load
    credits = Hash.new(0)
    self.courses.each do |course|
      credits[course.department] += course.credits
    end
    credits
  end
end
